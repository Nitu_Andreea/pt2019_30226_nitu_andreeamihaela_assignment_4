package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import business.Restaurant;

public class TableView extends View{
	ArrayList<JButton> tables;
	ImageIcon img=new ImageIcon("table.jpg");
	
	
	public TableView(ArrayList<JButton> table) {
		
		super("Choose a table","floor.jpg",new Dimension(600,300));
		this.tables=table;
		placeOnTheScreen();
		
	
		
}

	public void placeOnTheScreen() {
		GridLayout grid = new GridLayout(4,5);
		screen.setLayout(grid);

		
	//first row
		int i,j;
		for( i=0;i<tables.size()/2;i++) {
			screen.add(new JLabel(img));
		}
		for( i=0;i<tables.size()/2;i++) {
		
			tables.get(i).setBackground(Color.green);
			
			tables.get(i).setFont(new Font("Script MT Bold", 1, 30));
			screen.add(tables.get(i));
		}	
		//second row
		for(j=i;j<tables.size();j++) {
			screen.add(new JLabel(img));
		}
		for(j=i;j<tables.size();j++) {
			tables.get(j).setBackground(Color.green);
			
			tables.get(j).setFont(new Font("Script MT Bold", 1, 30));
			screen.add(tables.get(j));
		}
		
	}
	public void setFree(int i) {
		tables.get(i + Restaurant.N_TABLE).setBackground(Color.green);	
	}
	public void setBusy(int i) {
		tables.get(i + Restaurant.N_TABLE).setBackground(Color.red);	
	}
	public void allowAllBtns() {
		for(JButton b:tables) 
			b.setEnabled(true);
	}
	public void allowJustGreenBtns() {
		for(JButton b:tables)
			if (b.getBackground()==Color.red)b.setEnabled(false);
	}
	public void allowJustRedBtns() {
		for(JButton b:tables)
			if (b.getBackground()==Color.green)b.setEnabled(false);
	}
}
