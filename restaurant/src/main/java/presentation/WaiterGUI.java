package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import business.BaseProduct;
import business.MenuItem;
import business.Restaurant;

public class WaiterGUI extends View {
	TableView tableView;
	ArrayList<JButton> tables = new ArrayList<JButton>();
	private int orderTable;
	Color c = new Color(241, 196, 15);
	public JButton orderBtn = new JButton("Create Order");
	public JButton infoBtn = new JButton("Info Zone");
	public JButton ok=new JButton("Order is ready");
	public JPanel catalog = new JPanel();
	public JTable tabel;
	public JButton billBtn = new JButton("Create Bill");
	private JTextArea textZone=new JTextArea();
	private  JScrollPane hashPane=new JScrollPane(textZone);
	private int billTable;
	
	private void clickInTable() {
		
		this.tabel.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if ( !e.getValueIsAdjusting()  ) {
				int row = tabel.getSelectedRow();
				int col = tabel.getColumnCount()-1;
				try {
					
					int currentValue = Integer.parseInt(tabel.getValueAt(row, col).toString());// initial is
																									// zero,then it will
																									// increment
					
					tabel.setValueAt(currentValue + 1, row, col);
				} catch (Exception eee) {

				}
			}}
		});
	}
	public WaiterGUI(JTable tabel, Restaurant restaurant) {
		super("WaiterGUI", "waiter.jpeg", null);
		placeButtons();
		this.tabel = tabel;
		putTable(this.tabel);
		createTablesView();
		tableView = new TableView(tables);
		putHashJScrollPane();
		hashPane.setVisible(false);
		 clickInTable();

	}
	private void putHashJScrollPane() {
		hashPane.setVisible(true);
		hashPane.setBounds(650, 100, 700,700);
		textZone.setFont(new Font("Script MT Bold", 1, 16));
		getScreen().add(hashPane);

	}

	public void hideOrSeeHashMap(ArrayList<String> list) {
		if(hashPane.isVisible()) hashPane.setVisible(false);
		else
		{
			 hashPane.setVisible(true);
			printHashMap(list);
		}
	}
	
	public void printHashMap(ArrayList<String> list) {
		textZone.setText("");
		for(String s:list)
		textZone.append(s+"\n_______________________________________________________________________");
	
	}
	public void createTablesView() {
		for (int i = 0; i < Restaurant.N_TABLE; i++) {
			tables.add(new JButton(i + 1 + ""));
		}

	}

	public void prepareTableView() {
		// check first which is free
		for (int i = 0; i < Restaurant.N_TABLE; i++) {
			if (tables.get(i).getBackground() != Color.green) {
				tableView.setFree(i);
			} else if ( tables.get(i).getBackground() != Color.red) {
				tableView.setBusy(i);
			}
		}

	}

	public void showTableView() {
		prepareTableView();// apoi set visible si vezi sa fie adaugate

	}

	public void putJTable(JTable tabel) {
		if (js != null)
			getScreen().remove(js);
		this.tabel = tabel;
		js = new JScrollPane(this.tabel);
		js.setBounds(700, 10, 650, 670);
		getScreen().add(js);
	}

	private void placeButtons() {
		int size = 40;
		 // plasam cele 2 butoane si le realizam design-ul prin superclasa
		orderBtn = buttonStyle(orderBtn, size, c);
		infoBtn = buttonStyle(infoBtn, size, c);
		billBtn = buttonStyle(billBtn, size, new Color(99, 198, 247));
		ok = buttonStyle(ok, 30, c);
		
		int x = 220;
		int y = 400;
		int space = 100;

	
		orderBtn.setBounds(x, y + space, 400, 50);
		infoBtn.setBounds(x, y + 2 * space, 400, 50);
		billBtn.setBounds(x, y + 3 * space, 400, 50);
		
		ok.setBounds(800,800,400,60);
		ok.setVisible(false);
		
		getScreen().add(ok);
		getScreen().add(orderBtn);
		getScreen().add(infoBtn);
		getScreen().add(billBtn);
	}

	public void swapVisibilityCatalog() {
		if (billBtn.isVisible()) {
			billBtn.setVisible(false);
			js.setVisible(false);
		} else {
			billBtn.setVisible(true);
			js.setVisible(true);
		}

	}

	public JTable getComandTable() {
		return tabel;
	}

	public int getMaxIdFromTable() {
		return Integer.parseInt(tabel.getValueAt(tabel.getRowCount(), 0).toString());
	}

	public int getNowTable() {
		return orderTable;
	}

	public void setNowTable(int nowTable) {
		this.orderTable = nowTable;
	}

	public void markBusyTable(int i) {
		setNowTable(i);
	//	restaurant.freeTable[i] = false;
	}

	public void markFreeTable(int i) {
	//	setNowTable(i);
	//	restaurant.freeTable[i] = true;
	}
	public int getBillTable() {
		return billTable;
	}
	public void setBillTable(int billTable) {
		this.billTable = billTable;
	}
	
	
	public boolean isJTableOrderValid() {
		boolean emptyOrder=true; //vrem o comanda care sa nu fie goala
		for(int i=0;i<tabel.getRowCount();i++) {
			if(tabel.getValueAt(i,5)!="0"&& tabel.getValueAt(i,5)!=""&&tabel.getValueAt(i,5)!=null) {
				
				try {
					if(Integer.parseInt(tabel.getValueAt(i,5).toString())!=0) {
						emptyOrder=false;
					}
					
					
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null, "The field ["+tabel.getValueAt(i,5).toString()+"] could not be recorded.Be sure that you insert a number.");
					return false;
				}
				
		}}
		if(emptyOrder) {
			JOptionPane.showMessageDialog(null,"Empty order.");
			return false;
		}
		return true;
		
	}
	public ArrayList<MenuItem> getTheOrderFromTable(Restaurant restaurant) { /**BaseProduct*/
		//parcurg tabelul si vad daca sunt pe o linie care are MenuItems, atunci daca ultima coloana nu e zero, adaug in lista
		ArrayList<MenuItem> list=new ArrayList<MenuItem>();
		for(int i=0;i<tabel.getRowCount();i++) {
			if(tabel.getValueAt(i,5)!="0"&& tabel.getValueAt(i,5)!=""&&tabel.getValueAt(i,5)!=null) {
				
			
					int nr=Integer.parseInt(tabel.getValueAt(i,5).toString());
					for(int j=0;j<nr;j++) {
						//list.add(new BaseProduct(Integer.parseInt(tabel.getValueAt(i,0).toString()),tabel.getValueAt(i,1).toString(),Double.parseDouble(tabel.getValueAt(i,2).toString()),Integer.parseInt(tabel.getValueAt(i,3).toString()),Integer.parseInt(tabel.getValueAt(i,4).toString())));
						MenuItem item=restaurant.findMenuItem(Integer.parseInt(tabel.getValueAt(i,0).toString()));
						list.add(item);
				
				
		}
		
	}
		}
		return list;


}
	public void resetTable() {
		for(int i=1;i<tabel.getRowCount();i++) {
			if(tabel.getValueAt(i,0)!="")tabel.setValueAt("0", i, 5);
	}
	}
	public void cancelCurrentOrder() {
		resetTable();
		disappearJTable();
		ok.setVisible(false);
		//cancel the table selected
		tables.get(getNowTable()).setBackground(Color.green);
		
	}
	
	
	}
