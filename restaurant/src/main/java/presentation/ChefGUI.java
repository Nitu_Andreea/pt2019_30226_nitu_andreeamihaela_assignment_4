package presentation;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends View implements Observer {
	public ChefGUI() {
		super("Now chef is preparing your order!","chef.gif",new Dimension(500,340));
	}
	public void update(Observable arg0, Object arg1) {
		this.appearOnScreen();
	}

}
