package presentation;



import javax.swing.*;

public class MainView extends View{
	JButton adminBtn=new JButton("Administator");
	JButton waiterBtn=new JButton("Waiter");
	public MainView() {
		super("Main View","backMain.png",null);
		ImageIcon img=new ImageIcon("main.gif");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel jl=new JLabel(img);
		screen.add(jl);
		jl.setBounds(100,200,500,500);
		
		screen.add(adminBtn);
		adminBtn=this.buttonStyle(adminBtn);
		adminBtn.setBounds(900, 320, 300, 100);
		waiterBtn.setBounds(900,450, 300, 100);
		waiterBtn=this.buttonStyle(waiterBtn);

		screen.add(adminBtn);
		screen.add(waiterBtn);
		this.appearOnScreen();
		
	}
	
}
