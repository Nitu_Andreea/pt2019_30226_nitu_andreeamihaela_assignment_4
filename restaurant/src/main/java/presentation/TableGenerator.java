
package presentation;

import java.awt.Color;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

public class TableGenerator {
	 
	public static JTable createTableMeniu(ArrayList<MenuItem> orderr, ArrayList<String> typesList) {
		ArrayList<MenuItem> order=new ArrayList<MenuItem>(); //we clone the order, otherwise it will change the admin table
		for(MenuItem m:orderr) {
			if(m instanceof BaseProduct)order.add(new BaseProduct(m.getId(),m.getName(),m.getPrice(),m.getWeight(),m.getType()));
			else {
				//este un produs compus si trebuie sa ii luam si lista de produse
				ArrayList<MenuItem>array=((CompositeProduct) m).menuList;
				order.add(new CompositeProduct(m.getId(),m.getName(),m.getPrice(),m.getWeight(),m.getType(),array));
			}
		}
		order.sort(new Comparator<MenuItem>() {	//sortam dupa tip=>apar in meniu frumos
			public int compare(MenuItem a, MenuItem b) {
				return a.getType() - b.getType();
			}

		});
		ArrayList<String> fieldsMenu = new ArrayList<String>();
		fieldsMenu.add("ID");
		fieldsMenu.add("Name");
		fieldsMenu.add("Price");
		fieldsMenu.add("Weight");
		fieldsMenu.add("Type");
		fieldsMenu.add("Add");

		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(fieldsMenu.toArray());

		JTable table = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return column == 5;
			};
		};
		table.setModel(model);

		JTableHeader th = table.getTableHeader();
		TableColumnModel tcm = th.getColumnModel();
		for (int i = 0; i < fieldsMenu.size(); i++) {
			TableColumn tc = tcm.getColumn(i);
			tc.setHeaderValue(fieldsMenu.get(i));
		}

		String info[] = new String[fieldsMenu.size()];
		int type = 0;
		info[1] = typesList.get(order.get(0).getType()); //scriem primul titlu (pt ca nu necesita spatiu se trateaza separat)
		model.addRow(info);
	
		for (MenuItem m : order) { // iau toate elementele din meniu
			info[4] = m.getType() + ""; // daca avem un tip nou, scriem titlul asociat tipului
			
			if (type != m.getType()) {
				type = m.getType(); //avem un nou tip=>trebuie afisat titlul si lasat spatiu inainte
				//spatiu
				for(int i=0;i<=5;i++) info[i]="";
				model.addRow(info);
				//pune tiltul
				info[0] ="";
				info[1] = typesList.get(m.getType());//titlu
				for (int i = 2; i <= 5; i++)
					info[i] = "";
				model.addRow(info);
				info[0] = m.getId() + "";
				info[1] = m.getName();
				info[2] = m.getPrice() + "";
				info[3] = m.getWeight() + "";
				int k=m.getType()+1;
				info[4] =k + "";
				info[5] = "0";
				
			} else {
				
				info[0] = m.getId() + "";
				info[1] = m.getName();
				info[2] = m.getPrice() + "";
				info[3] = m.getWeight() + "";
				int k=m.getType()+1;
				info[4] =k + "";
				info[5] = "0";

			}
			model.addRow(info);
		}
		Font font = new Font("Monotype Corsiva", 1, 22);

		table.setFont(font);
		table.getTableHeader().setFont(new Font("Monotype Corsiva", 1, 26));
		table.setRowHeight(60);
		table.setBackground(Color.WHITE);
		table.setForeground(Color.black);
		for (int i = 1; i < table.getColumnCount(); i++)
			table.getColumnModel().getColumn(i).setMinWidth(150);

		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(3).setMinWidth(30);
		table.getColumnModel().getColumn(4).setMinWidth(30);
		table.getColumnModel().getColumn(5).setMinWidth(30);
		return table;

	}
	
	public static JTable createAdminMeniu(ArrayList<MenuItem> order, ArrayList<String> typesList) {

		
		ArrayList<String> fieldsMenu = new ArrayList<String>();
		fieldsMenu.add("ID");
		fieldsMenu.add("Name");
		fieldsMenu.add("Price");
		fieldsMenu.add("Weight");
		fieldsMenu.add("TypeId");
		fieldsMenu.add("TypeName");

		DefaultTableModel model = new DefaultTableModel() {
			  @Override
			    public boolean isCellEditable(int row, int column) {
			       return false;
			    }
		};
		model.setColumnIdentifiers(fieldsMenu.toArray());
	
		String[]info=new String[5];
		for(int i=0;i<5;i++) {
			info[i]=new String("");
		}
		JTable table = new JTable();
		table.setModel(model);
		for(int i=0;i<order.size();i++)
		{
			model.addRow(info);
		}
		JTableHeader th = table.getTableHeader();
		TableColumnModel tcm = th.getColumnModel();
		for (int i = 0; i < fieldsMenu.size(); i++) {
			TableColumn tc = tcm.getColumn(i);
			tc.setHeaderValue(fieldsMenu.get(i));
		}

		Font font = new Font("Monotype Corsiva", 1, 22);

		table.setFont(font);
		table.getTableHeader().setFont(new Font("Monotype Corsiva", 1, 26));
		table.setRowHeight(60);
		table.setBackground(Color.WHITE);
		table.setForeground(Color.black);
		for (int i = 1; i < table.getColumnCount(); i++)
			table.getColumnModel().getColumn(i).setMinWidth(150);

		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(3).setMinWidth(30);
		table.getColumnModel().getColumn(4).setMinWidth(30);
		table.getColumnModel().getColumn(5).setMinWidth(30);
		return table;

	}
	
}
