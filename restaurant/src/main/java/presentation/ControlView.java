package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import business.CompositeProduct;
import business.MenuItem;
import business.Order;
import business.Restaurant;
import data.FileWriter;

public class ControlView {
	Restaurant restaurant = new Restaurant();
	AdminGUI adminGUI;
	ChefGUI chefGUI = new ChefGUI();
	MainView mainView = new MainView();
	WaiterGUI waiterGUI = new WaiterGUI(TableGenerator.createTableMeniu(restaurant.meniu, Restaurant.typesList),
			restaurant);

	public ControlView() {
		// restaurant.putItems(); //uncomment this in case of initialization the
		// serialization
		restaurant.addObserver(chefGUI);
		adminGUI = new AdminGUI(TableGenerator.createAdminMeniu(restaurant.meniu, Restaurant.typesList));

		adminGUI.refreshTable(restaurant.meniu);
		addMainViewListeners();
		addAdminListeners();
		addAdminTableListener();
		addSerializationSave();

	}

	public void addSerializationSave() {
		mainView.jf.addWindowListener(new java.awt.event.WindowAdapter() { // before we close the program, we ask if we
																			// want to save our data
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(null, "Do you want to udate the data file?", "Close Window?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					restaurant.finishSerialization();
				}

			}
		});
	}

	private void addMainViewListeners() {
		mainView.adminBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				adminGUI.appearOnScreen();
			}

		});
		mainView.waiterBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				waiterGUI = new WaiterGUI(TableGenerator.createTableMeniu(restaurant.meniu, Restaurant.typesList),
						restaurant);
				waiterGUI.appearOnScreen();
				addWaiterListeners();

			}

		});
	}

	public void addWaiterListeners() {
		waiterGUI.infoBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				waiterGUI.hideOrSeeHashMap(restaurant.getPrintedHashMap());
			}
		});
		waiterGUI.orderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				waiterGUI.tableView.allowAllBtns();
				waiterGUI.tableView.appearOnScreen();
				waiterGUI.tableView.allowJustGreenBtns();
			}

		});

		waiterGUI.billBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				waiterGUI.tableView.allowAllBtns();
				waiterGUI.tableView.appearOnScreen();
				waiterGUI.tableView.allowJustRedBtns();
			}

		});
		// for each table where clients are we add the same listener: we make a bill or
		// a order
		ActionListener a = new ActionListener() {

			public void actionPerformed(ActionEvent e) { // inregistrare Order(timp,masa,id)
				JButton j = (JButton) e.getSource();
				int index = Integer.parseInt(j.getText()) - 1;
				if (j.getBackground() == Color.green) {
					j.setBackground(Color.red);
					waiterGUI.markBusyTable(index); // -1 pt ca indexul incepe de la 0, nu de la 1 cum e afisat
					JOptionPane.showMessageDialog(null, "Now go and create the order for table " + j.getText());
					// aici ar trebui sa se faca obtinerea a datei curente
					Order order = new Order(new Date(), index);
					restaurant.order[index] = order; // marcam ocuparea masei cu toate informatiile
					waiterGUI.setNowTable(index);
					waiterGUI.appearJTable();// in timp ce fereastra cu mesele dispare, apare optiunea de alegere a
												// comenzii din meniu
					waiterGUI.ok.setVisible(true);
				} else {// record the bill
					j.setBackground(Color.green);
					Order order = restaurant.order[index];
					// marchez index-ul pentru factura
					waiterGUI.setBillTable(index);

					// iau din restaurant factura si o scriu in fisier
					FileWriter.writeOrder(restaurant.generateBill(order));
					JOptionPane.showMessageDialog(null,
							"The bill for table " + j.getText() + "has been printed.Check the file ");

				}
				waiterGUI.tableView.disappearOnScreen();

			}

		};

		for (int i = 0; i < waiterGUI.tableView.tables.size(); i++) {
			waiterGUI.tableView.tables.get(i).addActionListener(a);
		}

		// add Listener for Order is ready: the Jtable should dissapear and we will
		// recive a message to know the order is recorded
		waiterGUI.ok.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (waiterGUI.isJTableOrderValid()) { // doar daca am scris valida comanda se inregistreaza : se
														// verifica sa nu fie goala si sa nu fie campuri care nu sunt
														// int-uri
					// se inregistreaza comanda din tabel
					// we need to add it to the hashMap
					ArrayList<MenuItem> list = waiterGUI.getTheOrderFromTable(restaurant);
					String afisList = restaurant.getPrettyOrderString(restaurant.order[waiterGUI.getNowTable()], list);
					int k = JOptionPane.showConfirmDialog(null, "Is this the complete order? \n" + afisList);
					if (k == 0) {
						restaurant.createOrder(restaurant.order[waiterGUI.getNowTable()], list);
						// the chef is cooking
						// chefGUI.appearOnScreen();
						restaurant.talkWithChef(list);
						// finally
						waiterGUI.resetTable();
						waiterGUI.disappearJTable();
						waiterGUI.ok.setVisible(false);
					} else if (k == 2) { // anulam comanda
						waiterGUI.cancelCurrentOrder();
					}

				} else {
					if (JOptionPane.showConfirmDialog(null, "Do you want to cancel the order?") == 0) {
						waiterGUI.cancelCurrentOrder();
					}

				}

			}

		});
		addMouseListener();

	}

	private void addMouseListener() { // nu-mi place ca la fiecare click se verifica. poate fac altfel
		waiterGUI.jf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (chefGUI.getScreen().isVisible())
					chefGUI.disappearOnScreen();

			}
		});

	}

	private void addAdminListeners() {
		adminAddLis();
		adminUpdateLis();
		adminDelLis();
		adminDelTypeLis();
		adminNewTypeLis();
	}

	private void adminAddLis() {// vreau sa fac resize la frame
		adminGUI.adaugare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int k = View.showTwoOptionOneQuestion("What kind of product do you want to add?", "Base product",
						"Composite Product");
				if (k != 0) {

					// Atunci am ales composite =>avem de ales in plus ce menuItems punem
					adminGUI.jAdd.add(adminGUI.compositeAdd);

					// adminGUI.jAdd.resize(new Dimension(1500, 800));
					adminGUI.putCompositeWindow(
							TableGenerator.createTableMeniu(restaurant.meniu, Restaurant.typesList));
					adminGUI.okAdd.setVisible(false); // aici folosim alt buton de validare
				} else {
					adminGUI.okAdd.setVisible(true);
					adminGUI.jAdd.remove(adminGUI.compositeAdd);
					// adminGUI.jAdd.resize(new Dimension(1500, 100));

				}
			}

		});
		// cand e gata, inregistrez obiectele din tabel
		adminGUI.compositeOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MenuItem m = adminGUI.getTheCompositeProduct(restaurant.meniu);
				System.out.println(m);
				if (m != null) {
					
					
						restaurant.meniu.add(m);
						adminGUI.cleanUpdateOrAddFields(adminGUI.addList);
						adminGUI.jfAdd.setVisible(false);
						adminGUI.refreshTable(restaurant.meniu);
					
				} else {
					JOptionPane.showMessageDialog(null,
							"You can't add a composite product without choosing its composition!");

			}
			}});
		adminGUI.okAdd.addActionListener(new ActionListener() {

	public void actionPerformed(ActionEvent arg0) {
		MenuItem m = adminGUI.getTheBaseProduct(restaurant.meniu);
		if (m != null) {

			restaurant.meniu.add(m);
			adminGUI.cleanUpdateOrAddFields(adminGUI.addList);
			adminGUI.jfAdd.setVisible(false);
			adminGUI.refreshTable(restaurant.meniu);

		}

	}

	});}

	private void adminUpdateLis() {
		adminGUI.update.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				adminGUI.updateList.get(0).setEditable(true); // reactivam scrierea pt id si stergem ce a fost
				adminGUI.cleanUpdateOrAddFields(adminGUI.updateList);

			}
		});
		adminGUI.okUpdate.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// facem update in restaurant
				// find the item with specific id
				try {
					MenuItem item = restaurant.findMenuItem(Integer.parseInt(adminGUI.updateList.get(0).getText()));
					// take the edited object
					MenuItem editedItem = adminGUI.getTheUpdateOrAddFields(adminGUI.updateList);
					if (editedItem.getType() > Restaurant.typesList.size()) {
						JOptionPane.showMessageDialog(null, "Undefined type of food!");
					} else {
						if (editedItem != null) {
							// update now
							restaurant.editMenuItem(item, editedItem);
							// refresh table
							adminGUI.refreshTable(restaurant.meniu);
							// hide jframe
							adminGUI.jfUpdate.setVisible(false);

						}
					}
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Undefined type of food");
				}

			}
		});
		setListenerToIdField(); // for update autocomplete
	}

	private void adminDelLis() {
		adminGUI.okDel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					int id = Integer.parseInt(adminGUI.idDel.getText());
					restaurant.deleteFromMenu(id); // id-1 pentru ca indexul in array=id citit-1 (incepe de la 0 in
													// array)s
					// reset id field
					adminGUI.idDel.setText("");

					adminGUI.refreshTable(restaurant.meniu);
					// hide jframe
					adminGUI.jfDel.setVisible(false);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Error:undefined id");
					exc.printStackTrace();
					adminGUI.idDel.setText("");
				}

			}
		});
	}

	private void adminNewTypeLis() {
		adminGUI.newType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				adminGUI.typeName.setText("");
				JOptionPane.showMessageDialog(null, restaurant.getPrintedMenuTypes());
				adminGUI.setNewTypeId(Restaurant.typesList.size() + 1);
			}

		});

		adminGUI.okType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String newType = adminGUI.getTypeName();
				Restaurant.typesList.add(newType);
				JOptionPane.showMessageDialog(null, restaurant.getPrintedMenuTypes());
				adminGUI.jfType.setVisible(false);
			}

		});
	}

	private void adminDelTypeLis() {
		adminGUI.deleteType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// first let me see all the types
				adminGUI.idTypeDelete.setText("");
				JOptionPane.showMessageDialog(null, "Remember the count number of the item you want to delete: \n"
						+ restaurant.getPrintedMenuTypes());
				adminGUI.jfDelType.setVisible(true);
			}

		});
		adminGUI.okDelType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// take the type and delete that
				int maxType = restaurant.getMaxType() + 1;
				if (Restaurant.typesList.size() > maxType) { // trebuie sa ne asiguram ca avem minim nr de tipuri care
																// exista deja. Daca stergem mai multe decat
																// sunt=>EXCEPTIE
					try {
						int i = Integer.parseInt(adminGUI.idTypeDelete.getText()) - 1;
						restaurant.deleteMenuType(i);
						JOptionPane.showMessageDialog(null,
								"Actualization complete: \n" + restaurant.getPrintedMenuTypes());
						adminGUI.jfDelType.setVisible(false);
						adminGUI.refreshTable(restaurant.meniu);
					} catch (Exception eee) {
						JOptionPane.showMessageDialog(null, "Error:undefined id");
					}

				} else {
					JOptionPane.showMessageDialog(null, "You must have at least " + maxType
							+ " types. These are already used in menu. Update their max type if you want to erase others.");
				}

			}
		});
	}

	private void setListenerToIdField() {
		adminGUI.updateList.get(0).addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					int id = Integer.parseInt(adminGUI.updateList.get(0).getText());
					MenuItem updateMe = restaurant.findMenuItem(id);
					if (updateMe == null) {
						JOptionPane.showMessageDialog(null, "Error: The object you try to find doesn't exist!");
					} else {
						adminGUI.completeTheUpdateFields(updateMe);
					}
				} catch (Exception xe) {
					JOptionPane.showMessageDialog(null, "Error:undefined id!");
				}
			}

		});

	}

	private void addAdminTableListener() {
		adminGUI.jtable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {

				MenuItem selectedItem = restaurant.meniu.get(adminGUI.jtable.getSelectedRow());
				if (adminGUI.jtable.getSelectedColumn() == 1)
					if (selectedItem instanceof CompositeProduct)
						JOptionPane.showMessageDialog(null, selectedItem.seeAll());
			}
		});
	}

}
