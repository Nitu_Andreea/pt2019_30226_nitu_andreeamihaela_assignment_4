package presentation;

import java.awt.*;
import javax.swing.*;

public abstract class View {
	protected Dimension d = new Dimension(1430, 1000);
	protected Font myFont = new Font("Script MT Bold", 1, 36);
	protected Color color = new Color(252, 226, 40);
	protected Color textColor = Color.black;
	protected ImageIcon img = new ImageIcon("img.png");
	protected JFrame jf;
	protected JLabel screen = new JLabel();
	protected JScrollPane js;

	public void putTable(JTable tabel) {
		js = new JScrollPane(tabel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		js.setVisible(false);
		js.setBounds(650, 100, 700, 700);
		getScreen().add(js);
	}

	public void appearJTable() {
		js.setVisible(true);
	}

	public void disappearJTable() {
		js.setVisible(false);
	}

	public View(String frameName, String imgPath, Dimension d) {
		if (d != null)
			this.d = d;
		this.img = new ImageIcon(imgPath); // setez imaginea specifica fiecarui frame
		fixFrame(frameName);
	}

	public static int showTwoOptionOneQuestion(String question,String s1,String s2) {
		Object[] options = { s1, s2 };
		int n=JOptionPane.showOptionDialog(null, question, "Choose",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
				options, // the titles of buttons
				options[0]); // default button title
		return n;
	}

	public JFrame fixFrame(String text) { // RUTINA DE INITIALIZARE A FERESTREI:
		setJf(new JFrame(text)); // dimensiune, imagine de fundal, culoare de fundal, vizibilitate
		setJf(new JFrame(text));
		getJf().setSize(d);

		getJf().setContentPane(getScreen());
		getScreen().setIcon(img);

		getJf().setVisible(false);
		getJf().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // si inchidere partiala a programului
		return getJf();
	}

	// METODE SPECIFICE DE DESIGN PENTRU PROIECT
	public JPanel getBox(String s, JTextField o) { // RETURNEAZA O COMPONENTA JPANEL, unind un textField si eticheta sa
		o = frameStyle(o);
		JLabel jl = new JLabel(s);
		o.setFont(new Font("Script MT Bold", 1, 25));
		jl.setFont(new Font("Script MT Bold", 1, 25));
		JPanel jp = new JPanel();
		jp.setBackground(new Color(249, 231, 159));
		jp.add(jl);
		jp.add((Component) o);
		jp.setSize(30, 60);
		return jp;
	}

	public JPanel getBox2(String s, JTextField o, JLabel waitCasa) {
		JPanel jp = getBox(s, o);
		JLabel jl2 = waitCasa;
		jp.add(jl2);
		jp.setSize(30, 60);
		return jp;
	}

	public JTextField frameStyle(JTextField idTf) {
		idTf.setFont(myFont);
		return idTf;

	}

	public JButton buttonStyle(JButton btn, int size, Color c) { // DESIGN BUTTON
		JButton jb = btn;
		if (c == null)
			jb.setBackground(color);
		else
			jb.setBackground(c);

		jb.setFont(new Font("Script MT Bold", 1, size));
		jb.setForeground(textColor);
		return jb;
	}

	public JButton buttonStyle(JButton btn) { // DESIGN BUTTON
		JButton jb = btn;
		jb.setBackground(color);
		jb.setFont(myFont);
		jb.setForeground(textColor);
		return jb;
	}

	public void appearOnScreen() {
		getJf().setVisible(true);
	}

	public void disappearOnScreen() {
		getJf().setVisible(false);
	}

	public JFrame getJf() {
		return jf;
	}

	public void setJf(JFrame jf) {
		this.jf = jf;
	}

	public JLabel getScreen() {
		return screen;
	}

	public void setScreen(JLabel screen) {
		this.screen = screen;
	}

}
