package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.html.HTMLDocument.Iterator;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;
import business.Restaurant;

public class AdminGUI extends View {

	public JButton adaugare = new JButton("New Item");
	public JButton update = new JButton("Update");
	public JButton delete = new JButton("Delete");
	public JButton newType = new JButton("New Type");
	public JButton deleteType = new JButton("Delete Type");

	public JTable jtable;
	protected JFrame jfType, jfAdd, jfDel, jfUpdate, jfDelType;
	private JPanel jType, jDelType;
	JPanel jAdd;
	private JPanel jDel;
	private JPanel jUpdate;
	JPanel compositeAdd;
	public JButton okAdd, okDel, okUpdate, okType, okDelType, compositeOk;

	ArrayList<JTextField> addList = new ArrayList<JTextField>();
	ArrayList<JTextField> updateList = new ArrayList<JTextField>();
	public JTextField idDel;
	private JTextField typeId;
	JTextField typeName;
	JTextField idTypeDelete;

	JTable addTable = new JTable();
	protected JScrollPane js, jSAdd;

	public AdminGUI(JTable table) {
		super("AdminGUI", "admin.jpg", null);
		placeButtons();
		putTable(table);
		createFrames();
		addListeners();
		addList.add(new JTextField(3)); // id field must be there,althrough it is not on the screen,otherwise it will be
										// different from udate where it exists. If field order is different then we can
										// not generalize it.
		addFields("Name", new JTextField(10)); // aici punem campurile in arraylist cu denumirea lor
		addFields("Price", new JTextField(10));
		addFields("Weight", new JTextField(10));
		addFields("Type", new JTextField(10));

		updateFields("id", new JTextField(3));
		updateFields("Name", new JTextField(10));
		updateFields("Price", new JTextField(10));
		updateFields("Weight", new JTextField(10));
		updateFields("Type", new JTextField(10));

	}

	private void designFrames() {
		Dimension d = new Dimension(1500, 100);
		Dimension d2 = new Dimension(200, 100);

		jfType.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfDelType.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfAdd.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfDel.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jfUpdate.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		jfType.setSize(new Dimension(500, 100));
		jfDelType.setSize(d2);
		jfAdd.setSize(d);
		jfDel.setSize(d2);
		jfUpdate.setSize(d);

		jType.setBackground(Color.orange);
		jDelType.setBackground(Color.orange);
		jAdd.setBackground(Color.orange);
		jDel.setBackground(Color.orange);
		jUpdate.setBackground(Color.orange);

		Color c = new Color(99, 198, 247);
		okType = this.buttonStyle(okType, 20, c);
		okDelType = this.buttonStyle(okDelType, 20, c);
		okAdd = this.buttonStyle(okAdd, 20, c);
		compositeOk = this.buttonStyle(compositeOk, 20, c);
		okDel = this.buttonStyle(okDel, 20, c);
		okUpdate = this.buttonStyle(okUpdate, 20, c);
	}

	protected void addFields(String s, JTextField tf) {// de fiecare data cand vrem sa adaugam noi campuri
		addList.add(tf);
		JPanel jp = this.getBox(s, tf);
		jAdd.add(jp);
	}

	protected void updateFields(String s, JTextField tf) {
		updateList.add(tf);
		JPanel jp = this.getBox(s, tf);
		jUpdate.add(jp);
	}

	public void refreshTable(ArrayList<MenuItem> order) {
		java.util.Iterator<MenuItem> iterator = order.iterator();
		int i;
		for (i = 0; i < jtable.getRowCount() && iterator.hasNext(); i++) {
			MenuItem m = iterator.next();
			jtable.setValueAt(m.getId() + "", i, 0);
			jtable.setValueAt(m.getName() + "", i, 1);
			jtable.setValueAt(m.getPrice() + "", i, 2);
			jtable.setValueAt(m.getWeight() + "", i, 3);
			int k = m.getType() + 1; // tipurile sa inceapa de la 1
			jtable.setValueAt(k + "", i, 4);
			jtable.setValueAt(Restaurant.typesList.get(m.getType()), i, 5);
		}
		while (i < jtable.getRowCount()) {
			for (int j = 0; j < jtable.getColumnCount(); j++)
				jtable.setValueAt("", i, j);
			i++;
		}
		String info[] = new String[jtable.getColumnCount()];
		while (iterator.hasNext()) // inseamna ca trebuie sa adaugam noi item-uri
		{
			MenuItem m = iterator.next();
			info[0] = m.getId() + "";
			info[1] = m.getName();
			info[2] = m.getPrice() + "";
			info[3] = m.getWeight() + "";
			int k = m.getType() + 1;
			info[4] = k + "";
			info[5] = Restaurant.getTypeName(m.getType());
			DefaultTableModel dm = (DefaultTableModel) (jtable.getModel());
			dm.addRow(info);
		}
	}

	public void putTable(JTable tabel) {
		if (js != null)
			getScreen().remove(js);
		this.jtable = tabel;
		js = new JScrollPane(tabel);
		js.setVisible(true);
		js.setBounds(340, 120, 900, 770);
		getScreen().add(js);
	}

	public void putCompositeWindow(JTable tabel) {
		putAddTable(tabel);

	}

	public void putAddTable(JTable tabel) {
		if (jSAdd != null)
			compositeAdd.remove(jSAdd);
		this.addTable = tabel;
		jSAdd = new JScrollPane(tabel);
		jSAdd.setVisible(true);
		jSAdd.setBounds(0, 0, 700, 700);
		compositeAdd.add(jSAdd);
		clickInTable();
	}

	private void addListeners() {
		adaugare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfAdd.setVisible(true);

			}

		});
		delete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfDel.setVisible(true);

			}

		});
		update.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfUpdate.setVisible(true);

			}

		});
		newType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfType.setVisible(true);

			}

		});
		deleteType.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				jfDelType.setVisible(true);

			}

		});
	}

	protected void placeButtons() { // poate fi generalizata!
		adaugare = buttonStyle(adaugare, 30, null);
		update = buttonStyle(update, 30, null);
		delete = buttonStyle(delete, 30, null);
		newType = buttonStyle(newType, 30, null);
		deleteType = buttonStyle(deleteType, 27, null);

		int x = 200;
		int space = 250;
		adaugare.setBounds(x, 30, 200, 50);
		update.setBounds(x + space, 30, 200, 50);
		delete.setBounds(x + 2 * space, 30, 200, 50);
		newType.setBounds(x + 3 * space, 30, 200, 50);
		deleteType.setBounds(x + 4 * space, 30, 200, 50);

		screen.add(adaugare);
		screen.add(update);
		screen.add(delete);
		screen.add(newType);
		screen.add(deleteType);

	}

	public void createFrames() {
		jfType = new JFrame("Add Type");
		jfDelType = new JFrame("Delete Type");
		jfAdd = new JFrame("Adaugare");
		jfDel = new JFrame("Stergere");
		jfUpdate = new JFrame("Update");

		jType = new JPanel();
		jDelType = new JPanel();
		jAdd = new JPanel();
		jDel = new JPanel();
		jUpdate = new JPanel();

		okType = new JButton("ok");
		okDelType = new JButton("ok");
		okAdd = new JButton("ok");
		compositeOk = new JButton("Create product");
		okDel = new JButton("ok");
		okUpdate = new JButton("ok");

		compositeAdd = new JPanel();
		compositeAdd.setLayout(null);
		compositeAdd.setPreferredSize(new Dimension(700, 800));
		compositeOk.setBounds(40, 720, 600, 50);

		compositeAdd.add(compositeOk);

		// textfields for type
		typeId = new JTextField(3);
		typeId.setEditable(false);
		typeName = new JTextField(10);
		idTypeDelete = new JTextField(3);

		designFrames();

		idDel = new JTextField(3); // toate au id-ul comun, deci punem pt cele 3 frame-uri

		jAdd.add(okAdd);
		jDel.add(okDel);
		jUpdate.add(okUpdate);

		jDelType.add(okDelType);
		jType.add(okType);
		jType.add(this.getBox("id", typeId));
		jType.add(this.getBox("Name", typeName));

		jDelType.add(this.getBox("id", idTypeDelete));

		JPanel jp = this.getBox("id:", idDel); // la delete,update avem un singur camp.
		jDel.add(jp);

		jfType.setContentPane(jType);
		jfDelType.setContentPane(jDelType);
		jfAdd.setContentPane(jAdd);
		jfDel.setContentPane(jDel);
		jfUpdate.setContentPane(jUpdate);
	}

	public String getTypeName() {
		return typeName.getText();
	}

	public void setNewTypeId(int i) {
		typeId.setText(i + "");

	}

	public void completeTheUpdateFields(MenuItem updateMe) {
		updateList.get(0).setEditable(false);
		updateList.get(1).setText(updateMe.getName());
		updateList.get(2).setText(updateMe.getPrice() + "");
		updateList.get(3).setText(updateMe.getWeight() + "");
		int k = updateMe.getType() + 1;
		updateList.get(4).setText(k + "");

	}

	public void cleanUpdateOrAddFields(ArrayList<JTextField> fields) {
		for (JTextField jt : fields) {
			jt.setText("");
		}

	}

	public MenuItem getTheUpdateOrAddFields(ArrayList<JTextField> fields) { // work only for base //create a new item or
																			// update. It depends which ArrayList we
																			// sent as parameter

		String name = fields.get(1).getText();
		int type = -1, weight = 0;
		double price = 0;
		try {
			price = Double.parseDouble(fields.get(2).getText());
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Error:unable to read price");
			return null;
		}
		try {
			weight = Integer.parseInt(fields.get(3).getText());

		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Error:unable to read weight");
			return null;
		}
		try {
			type = Integer.parseInt(fields.get(4).getText()) - 1;
			if (type > Restaurant.typesList.size()) {
				JOptionPane.showMessageDialog(null, "Undefined type of food");
				return null;
			}
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Error:unable to read type");
			return null;
		}

		if (type == -1)
			return null;
		return new BaseProduct(name, price, weight, type);
	}

	private void clickInTable() {

		this.addTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					int row = addTable.getSelectedRow();
					int col = addTable.getColumnCount() - 1;
					try {
						
						int currentValue = Integer.parseInt(addTable.getValueAt(row, col).toString());// initial is
																										// zero,then it
																										// will
																										// increment

						addTable.setValueAt(currentValue + 1, row, col);
					} catch (Exception eee) {

					}
				}
			}
		});
	}

	private ArrayList<MenuItem> getTheCompositeFromTable(ArrayList<MenuItem> restaurant) {
		// this metods returns the object selected from the table: but what we have in
		// the table,we have in the arrayList, so:we need only the nr of objects that
		// need to be inserted and the index in table,beacause is the same as the index
		// of array
		ArrayList<MenuItem> items = new ArrayList<MenuItem>();
		int rows = addTable.getRowCount();
		int lastCol = addTable.getColumnCount() - 1;
		for (int i = 0; i < rows; i++) {
			try {
				int nrBucati = Integer.parseInt(addTable.getValueAt(i, lastCol).toString());
				int id = Integer.parseInt(addTable.getValueAt(i, 0).toString());
				
				if (nrBucati > 0) {
					for (int j = 0; j < nrBucati; j++) {
						MenuItem thisItem = restaurant.get(id - 1);
						items.add(thisItem);
					}

				}
			} catch (Exception e) { // because of the structure with types, it could be an empty value unabled to
									// parse function

			}
		}
		return items;
	}

	public MenuItem getTheCompositeProduct(ArrayList<MenuItem> restaurant) {
		ArrayList<MenuItem> list = getTheCompositeFromTable(restaurant);

		MenuItem fieldsItem = this.getTheUpdateOrAddFields(this.addList);
		if(fieldsItem==null)return null;
		
		if (list.size()>0)
			return new CompositeProduct(fieldsItem.getId(), fieldsItem.getName(), fieldsItem.getPrice(),
					fieldsItem.getWeight(), fieldsItem.getType(), list);
		return null;
	}

	public MenuItem getTheBaseProduct(ArrayList<MenuItem> restaurant) {

		MenuItem fieldsItem = this.getTheUpdateOrAddFields(this.addList);
		System.out.println(fieldsItem);
		if (fieldsItem==null) return null;
		if (fieldsItem.getType() < Restaurant.typesList.size()) {
			return new BaseProduct(fieldsItem.getId(), fieldsItem.getName(), fieldsItem.getPrice(),
					fieldsItem.getWeight(), fieldsItem.getType());
		} else
			return null;

	}

}
