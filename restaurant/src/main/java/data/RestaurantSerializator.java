package data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import business.MenuItem;
import business.Restaurant;

public class RestaurantSerializator {
	private static final String FILE_NAME = "RESTAURANTdata.txt";
	FileOutputStream streamFileOut;
	FileInputStream streamFileIn;

	ObjectOutputStream out;
	ObjectInputStream in;

	// -------------------------------------------------------------------------------------------
	public void writeMenu(ArrayList<MenuItem> list) {
		// openToWriteSerialzator();

		writeObject(new Integer(MenuItem.getI())); // altfel incepe iarasi de la 0 id-ul pentru ca e static
		for (MenuItem m : list) {
			writeObject(m);
		}
		closeWriteSerializator();

	}

	public void writeTypes() {
		openToWriteSerialzator();
		writeObject(new Integer(Restaurant.typesList.size())); // actualizam nr de tipuri
		for (String s : Restaurant.typesList) {
			writeObject(s);
		}
	}

	public ArrayList<String> readTypes() {
		ArrayList<String> list = new ArrayList<String>();
		String s;
		openToReadSerialzator();
		// citim id-ul care ne spune cate tipuri avem in total
		int nr = (Integer) readObject();
	
		for (int i = 0; i < nr; i++) {
			String st = (String) readObject();
			list.add(st);
		}
	
		return list;
	}


	public ArrayList<MenuItem> readMenu() {
		ArrayList<MenuItem> list = new ArrayList<MenuItem>();
		MenuItem m;
		// openToReadSerialzator();
		// citim id-ul care ne spune cate elemente avem in total
		int i = (Integer) readObject();
		MenuItem.setI(i);
		while ((m = (MenuItem) readObject()) != null) {
			list.add(m);
		}
		closeReadSerializator();
		return list;
	}

	// -------------------------------------------------------------------------------------------
	private void openToReadSerialzator() {
		try {
			streamFileIn = new FileInputStream(FILE_NAME);
			in = new ObjectInputStream(streamFileIn);

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Serializare imposibila(citire)");
		}
	}

	private void openToWriteSerialzator() {
		try {
			streamFileOut = new FileOutputStream(FILE_NAME);
			out = new ObjectOutputStream(streamFileOut);

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Serializare imposibila(scriere)");
		}
	}

	private void closeWriteSerializator() {
		try {
			out.close();
			streamFileOut.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Probleme la inchiderea serializarii(scriere)");
		}

	}

	private void closeReadSerializator() {
		try {
			in.close();
			streamFileIn.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Probleme la inchiderea serializarii(citire)");
		}

	}

	public void writeObject(Object object) {
		try {
			out.writeObject(object);

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Probleme in cursul serializarii(scriere)");
		}
	}

	private Object readObject() {
		Object o = null;
		try {
			try {
				o = in.readObject();
			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Class not found: serializare (citire)");
			}
		} catch (IOException e) {
			// JOptionPane.showMessageDialog(null, "Probleme in cursul
			// serializarii(citire)");
		}
		return o;

	}

}
