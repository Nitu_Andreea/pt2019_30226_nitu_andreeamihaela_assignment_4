package business;

import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.Iterator;

public class CompositeProduct extends MenuItem {
	public ArrayList<MenuItem> menuList = new ArrayList<MenuItem>();

	public CompositeProduct(String name, Double price, int weight, int type, ArrayList<MenuItem> items) {
		super(name, price, weight, type);
		for (MenuItem m : items) { // pun obiectele in lista proprie
			menuList.add(m);
		}
	}

	public CompositeProduct(int id, String name, Double price, int weight, int type, ArrayList<MenuItem> items) {
		super(id, name, price, weight, type);
		for (MenuItem m : items) { // pun obiectele in lista proprie
			menuList.add(m);
		}
	}

	public String seeAll() {
		String s = "";
		for (int i = 0; i < menuList.size(); i++) {
			int k = i + 1;
			s += "\n" + k + ") " + menuList.get(i).toString() + "\n";
		}
		return s;
	}

	public void removeAll(MenuItem it) {
		for (java.util.Iterator<MenuItem> iterator = menuList.iterator(); iterator.hasNext(); ) {
		    MenuItem value = iterator.next();
		    if ((value.getId() == it.id)) {
		        iterator.remove();
		    }
		}
		
	}

	@Override
	public double computePrice() {
		for (MenuItem m : menuList) {
			this.price += m.computePrice();
		}
		return 0;
	}
}
