package business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import javax.swing.JOptionPane;

import data.RestaurantSerializator;

public class Restaurant extends Observable implements RestaurantProcessing {
	public static final int N_TABLE = 10; // this should be 10 multiple
	public ArrayList<MenuItem> meniu = new ArrayList<MenuItem>();
	public static ArrayList<String> typesList = new ArrayList<String>(); // all my types of food

	public Order[] order = new Order[N_TABLE];// fiecare masa are cate un obiect Order asociat pentru ca sa se faca
												// hashing dupa obiect
	public HashMap<Order, ArrayList<MenuItem>> hashMap = new HashMap<Order, ArrayList<MenuItem>>();
	private RestaurantSerializator serializator = new RestaurantSerializator();
	// -------------------------------------------------------------------------------------------------------
	//CONSTRUCTOR
	/**
	 * @pre assertValidators()
	 **/
	public Restaurant() {
		assertValidators();
		startSerialization(); // comment this in case of initialization the serialization and uncomment in

	}

	/**
	 * @pre N_TABLE>0
	 * @pre N_TABLE%10==0
	 * @pre tableID>0
	 * @pre meniu.size()>0 && typesList.size()>0
	 * 
	 */
	private void assertValidators() {
		assert N_TABLE > 0 : "Numarul de mese nu poate fi negativ";
		assert N_TABLE % 10 == 0 : "Numarul de mese trebuie sa fie multiplu de 10!";
	
	}
	// -------------------------------------------------------------------------------------------------------
	//FROM INTERFACE
	/**
	 * @pre order!=null&&items!=null
	 **/
	public void createOrder(Order order, ArrayList<MenuItem> items) {
		assert order!=null&&items!=null :"Nu se poate adauga o comanda null";
		hashMap.put(order, items);

	}
	
	/**
	 * @pre items!=null
	 **/
	public double computeOrderPrice(ArrayList<MenuItem> items) {
		assert items!=null: "Nu poti face pretul la ceva ce nu exita";
		double price = 0;
		for (MenuItem m : items) {
			price += m.getPrice();
		}
		return price;
	}
	/**
	 * @pre order!=null
	 **/
	public String generateBill(Order order) {
		assert order!=null :"Nu poti face o factura la un order care nu exista";
		String bill = "THE BILL:\n";
		ArrayList<MenuItem> items = getTheItemsOf(order);
		bill += getPrettyOrderString(order, items);

		double price = computeOrderPrice(items);
		bill += "Total price: " + price;
		return bill;

	}
	// FROM INTERFACE : ADMIN

	public void createNewMenuItem(MenuItem menuItem) { //crearea propriu zisa am facut-o in interfata luand campurile
		meniu.add(menuItem);
	}

	public void deleteFromMenu(int id) {
		MenuItem m = null;
		for (MenuItem obj : this.meniu) {
			if (obj.id == id) {
				m = obj;
				break;
			}
		}
		if (m instanceof BaseProduct)
			this.removeAll(m);// daca e base, il stergem de tot unde mai e in composite
		meniu.remove(m);
		// actualizam var statica din MenuItem
		MenuItem.setI(MenuItem.getI() - 1);
	
	}

	public void editMenuItem(MenuItem menuItem) {
		// TODO Auto-generated method stub

	}
	

	public void deleteMenuType(int i) {
		typesList.remove(i);

	}
	// -------------------------------------------------------------------------------------------------------
	// HASH MAP
	/**
	 * @pre hashMap!=null
	 **/
	public ArrayList<String> getPrintedHashMap() {
		assert hashMap != null : "HashMap inexistent. Nu se poate realiza afisarea";
		ArrayList<String> s = new ArrayList<String>();
		for (Order order : hashMap.keySet()) {
			String prettyOrder = this.getPrettyOrderString(order, hashMap.get(order));
			s.add(prettyOrder);
		}
		return s;
	}

	// FROM INTERFACE : WAITER

	/**
	 * @pre hashMap!=null
	 **/

	public ArrayList<MenuItem> getTheItemsOf(Order order) {
		assert hashMap != null : "HashMap null. Nu se poate gasi comanda";
		return hashMap.get(order);
	}

	// -------------------------------------------------------------------------------------------------------
	// MENIU
	/**
	 * @pre meniu!=null
	 **/
	public MenuItem findMenuItem(int id) {
		assert meniu != null : "Meniu null. Nu se realizeaza cautarea";
		MenuItem updateMe = null;
		for (MenuItem it : meniu) {
			if (it.getId() == id) {
				updateMe = it;
				break;
			}
		}
		return updateMe;

	}
	/**
	 * @pre m!=null
	 **/
	private void removeAll(MenuItem m) {
		assert m!=null:"Produsul ce trebuie sters din toate listele nu exista.";
		
		for (MenuItem obj : this.meniu) {
			if (obj instanceof CompositeProduct)
				obj.removeAll(m);
		}
		
	}
	
	/**
	 * @pre index!=-1
	 **/
	public void editMenuItem(MenuItem item, MenuItem editedItem) {
		int index = meniu.indexOf(item);
		try {
		assert index != -1 : "Nu exista item-ul cautat pentru update";
		}catch(AssertionError  e){
			JOptionPane.showMessageDialog(null, "Undefined id!");
		}
		meniu.get(index).setName(editedItem.getName());
		meniu.get(index).setPrice(editedItem.getPrice());
		meniu.get(index).setWeight(editedItem.getWeight());
		meniu.get(index).setType(editedItem.getType());

	}

	/**
	 * @pre max!=-1
	 **/
	public int getMaxType() {
		int max = -1;
		for (MenuItem m : this.meniu) {
			if (m.getType() > max)
				max = m.getType();
		}
		assert max != -1 : "Nu exista un maxim pentru id-ul tipului";
		return max;
	}

	// -------------------------------------------------------------------------------------------------------
	//typesList
	

	// interface stops here

/**
	 * @pre typesList!=null
	 **/

	public static String getTypeName(int type) {
		try {
			assert typesList!=null : "typesList null!";
			assert type<=typesList.size() && type>=0: "Undefined type!";
			return typesList.get(type);
		}catch(AssertionError  e) {
			
		}
		return null;
		
	}
	

	public String getPrettyOrderString(Order order, ArrayList<MenuItem> list) {
		String afisList = order.toString() + "\n"; // we create a kind of toString for the ItemsOrdered --toStringClient
													
		for (int i = 0; i < list.size(); i++) {
			int k = i + 1;// pune numarul +1, pt ca nu exista masa 0,decat pt programator
			afisList += k + ". " + list.get(i) + "\n";
		}
		return afisList;
	}

	

/**
	 * @pre typesList!=null
	 **/

	public String getPrintedMenuTypes() {
		assert typesList!=null : "typesList null!";
		String s = "Menu types: \n";
		for (int i = 0; i < typesList.size(); i++) {
			int k = i + 1;
			s += k + ". " + typesList.get(i) + "\n";
		}
		return s;
	}

	//-----------------------------------------------------------------------------------------------------------
	//SERIALIZATION
	/**
	 * @pre meniu!=null
	 * @pre typesList!=null
	 **/

	public void finishSerialization() {
		assert meniu != null : "Nimic de serializat.Meniul este null";
		assert typesList != null : "Nimic de serializat.Lista de tipuri este null";
		serializator.writeTypes();
		serializator.writeMenu(meniu);
	}

	/**
	 * @post typesList!=null
	 * @post meniu!=null
	 **/

	public void startSerialization() {
		typesList = serializator.readTypes();
		meniu = serializator.readMenu();
		assert typesList != null && meniu != null : "Serializarea s-a executat degeaba. Nu exista nimic in fisier.Trebuie realizata initializarea serializarii. Urmeaza comentariile din constructorul claselor Restaurant si ControlView.";

	}
	//-----------------------------------------------------------------------------------------------------------
	//CHEF NOTIFY
	/**
	 * @pre order!=null
	 **/

	public void talkWithChef(ArrayList<MenuItem> order) {
		assert order != null : "Nu poti sa ii trimiti bucatarului o comanda goala. Se va supara!";
		boolean ok = false;
		for (MenuItem m : order) { // check if there is any compositeProduct
			if (m instanceof CompositeProduct) {
				ok = true;
				break;
			}
		}
		if (ok) {
			setChanged();
			this.notifyObservers();
		}

	}
	//-----------------------------------------------------------------------------------------------------------
	//INITIALIZATION
	public void putItems() {
		// add my types of food
		typesList.add("Racoritoare");// type 1
		typesList.add("Bauturi calde");// type 2
		typesList.add("Putin alcool");// type 3
		typesList.add("Salate");// type 4
		typesList.add("Garnituri");// type 5
		typesList.add("Carne");// type 6
		typesList.add("Desert");// type 7
		typesList.add("meniu");// type 8

		meniu.add(new BaseProduct("apa", 5.3, 50, 0));
		meniu.add(new BaseProduct("cartofi", 6.3, 50, 4));
		meniu.add(new BaseProduct("peste", 32.3, 710, 6));
		meniu.add(new BaseProduct("tort", 21.3, 500, 7));
		meniu.add(new BaseProduct("fresh de portocale", 12.3, 50, 0));
		meniu.add(new BaseProduct("ciocolata calda", 12.3, 5, 1));
		meniu.add(new BaseProduct("rucola", 12.3, 50, 3));
		meniu.add(new BaseProduct("vin", 12.3, 50, 2));

		ArrayList<MenuItem> list = new ArrayList<MenuItem>();
		ArrayList<MenuItem> list2 = new ArrayList<MenuItem>();

		list.add(meniu.get(0));
		list.add(meniu.get(1));
		list.add(meniu.get(2));

		list2.add(meniu.get(3));
		list2.add(meniu.get(5));

		meniu.add(new CompositeProduct("meniu peste", 52.3, 750, 7, list));
		meniu.add(new CompositeProduct("meniu dulce", 32.3, 500, 7, list2));
	}

	public double computeOrderPrice(Order order) {
		// TODO Auto-generated method stub
		return 0;
	}

}
