package business;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{
	private static int i=0;
	protected String name="";
	protected int type; //decide ordinea de afisare in meniu ex. tipul 1: salata tipul 2:carne, 3:bauturi
	protected int id;
	protected double price;
	protected int weight;
	
	private CompositeProduct product;
	public MenuItem(String name,Double price,int weight,int type) {
		i++;
		id=i;
		this.weight=weight;
		this.name=name;
		this.price=price;
		this.type=type;
	}
	public MenuItem(int id,String name,Double price,int weight,int type) {
		this.id=id;
		this.weight=weight;
		this.name=name;
		this.price=price;
		this.type=type;
	}
	public abstract double computePrice();
	
	public String toString() {
		return  Restaurant.getTypeName(type)+":"+name + ", id=" + id + ", weight=" + weight+", price=" + price ;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public static int getI() {
		return i;
	}
	public static void setI(int i) {
		MenuItem.i = i;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public CompositeProduct getProduct() {
		return product;
	}
	public void setProduct(CompositeProduct product) {
		this.product = product;
	}
	public String seeAll() {
		return null;
	}
	public void removeAll(MenuItem m) {
		
	}
	
	
}
