package business;

public class BaseProduct extends MenuItem {

	public BaseProduct(int i, String name, double price, int weight, int type) {
		super(i, name, price, weight, type);
	}

	public BaseProduct(String name, double price, int weight, int type) {
		super(name, price, weight, type);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public double computePrice() {
		return price;
	}
}
