package business;

import java.io.Serializable;
import java.util.Date;

import javax.swing.JTable;

public class Order implements Serializable{
	private static int i=0;
	private int orderID;
	private Date date=new Date();
	private int table;
	


	public Order(Date date2, int table) {
		this.orderID = i;
		i++;
		this.date = date2;
		this.setTable(table);
	}

	public int hashCode() {
		return (int) (orderID+table+date.getTime());
		
	}
	
	@Override
    public boolean equals(Object obj) 
    { 
           
    // 1. compare references
    if(this == obj) 
            return true; 
          
    //2.check if the class is the same    
    if(obj == null || obj.getClass()!= this.getClass()) 
            return false; 
          
        // 3.Compare each field
        Order order = (Order) obj; 
        return (order.orderID == this.orderID  && order.date.equals(this.date)&& order.table == this.table); 
    } 
      
      

  
	
	
	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public String toString() {
		int realTable=table+1;
		return "\n Data curenta:"+date.toString()+"\n"+"Numar masa:"+realTable+"\n"+"Comanda id:"+orderID; //toStringClient pune numarul +1, pt ca nu exista masa 0,decat pt programator

	}

	
}
