package business;

import java.util.ArrayList;

public interface RestaurantProcessing {
	public void createNewMenuItem(MenuItem menuItem);
	public void deleteFromMenu(int id);
	public void editMenuItem(MenuItem menuItem);
	
	public void createOrder(Order order,ArrayList<MenuItem> a);
	public double computeOrderPrice(Order order);
	public String generateBill(Order order);
}
